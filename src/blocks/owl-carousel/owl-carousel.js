$(document).ready(function() {

  $('#promo-slider').owlCarousel({
    items: 1,
    dots: false,
    autoplay: true,
    autoplayTimeout: 6000,
    smartSpeed: 600,
    loop: true
  });

  $('#products-slider').owlCarousel({
    margin: 30,
    stagePadding: 20,
    dots: false,
    nav: true,
    navText: ['<svg width="50px" height="20px" viewBox="0 0 50 20" xml:space="preserve"><g id="arrow-shape" fill="none" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polyline id="line" points=" 5,10 45,10" preserveAspectRatio="none"/><polyline id="arrow" points=" 37,2 45,10 37,18 " /></g></svg>', '<svg width="50px" height="20px" viewBox="0 0 50 20" xml:space="preserve"><g id="arrow-shape" fill="none" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polyline id="line" points=" 5,10 45,10" preserveAspectRatio="none"/><polyline id="arrow" points=" 37,2 45,10 37,18 " /></g></svg>'],
    responsive: {
      0: {
        items: 1
      },
      768: {
        items: 2
      },
      992: {
        items: 3
      },
      1200: {
        items: 4
      }
    }
  });

  $('#partners-slider').owlCarousel({
    dots: false,
    autoplay: true,
    autoplayTimeout: 4000,
    smartSpeed: 600,
    loop: true,
    responsive: {
      0: {
        items: 2
      },
      500: {
        items: 3
      },
      768: {
        items: 4
      },
      1200: {
        items: 5
      }
    }
  });
});
